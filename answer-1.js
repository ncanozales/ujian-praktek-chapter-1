const values = {
  name: 'John Doe',
  age: 21,
  username: 'johndoe',
  password: 'abc123',
  location: 'Jakarta',
  time: new Date(),
};

function mariKitaSerialkan(original) {
  var temp = Object.assign({}, original);
  delete temp['password'];
  delete temp['time'];
  return JSON.stringify(temp);
}

console.log(mariKitaSerialkan(values));
