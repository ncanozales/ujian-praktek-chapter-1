import React, { useEffect } from 'react';
import './App.css';
import { useState } from 'react';

function App() {
  const [message, setMessage] = useState('');
  const [capital, setCapital] = useState('');
  const [reverse, setReverse] = useState('');
  const [frequent, setFrequent] = useState('');
  const [char, setChar] = useState(0);
  const [word, setWord] = useState(0);

  const countFrequent = (str) => {
    var maks = 0,
      maksChar = '';
    str.split('').forEach(function (char) {
      if (str.split(char).length > maks) {
        maks = str.split(char).length;
        maksChar = char;
      }
    });
    maks--;
    return maksChar + ' (' + maks + ')';
  };

  const countWord = (str) => {
    const arr = str.split(' ');
    return arr.filter((word) => word !== '').length;
  };

  useEffect(() => {
    setCapital(message.toUpperCase());
    setReverse(message.split('').reverse().join(''));
    setFrequent(countFrequent(message));
    setChar(message.length);
    setWord(countWord(message));
  }, [message]);

  return (
    <div className='App'>
      <header className='App-header'>
        <h1>Input (No Need Button)</h1>
        <textarea
          cols='50'
          rows='5'
          value={message}
          onChange={(event) => {
            setMessage(event.target.value);
            console.log(event.target.value);
          }}
        ></textarea>

        <h1>Output</h1>
        <p>Capitalize Version : {capital}</p>
        <p>Reversed Version : {reverse}</p>
        <p>Most Frequent Character : {frequent}</p>
        <p>Words Count : {word}</p>
        <p>Characters Count : {char}</p>
      </header>
    </div>
  );
}

export default App;
