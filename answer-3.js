class World {
  constructor(num) {
    this.cities = [];
    for (let i = 0; i < num; i++) {
      this.cities.push(new City());
    }
  }

  add_city(name) {
    this.cities.push(new City(name));
  }

  random_city() {
    return this.cities[Math.floor(Math.random() * this.cities.length)];
  }

  total_cities() {
    return this.cities.length;
  }
}

class City {
  constructor(name) {
    if (name) {
      this.name = name;
    } else {
      let char = 'abcdefghijklmnopqrstuvwxyz';
      let charLength = char.length;
      let randomChar = '';
      for (var i = 1; i <= 5; i++) {
        randomChar += char.charAt(Math.floor(Math.random() * charLength));
      }
      this.name = randomChar;
    }
    this.citizens = this.add_citizen();
  }

  add_citizen() {
    return new Citizen().age;
  }
}

class Citizen {
  constructor() {
    this.age = parseInt(Math.floor(Math.random() * 101));
  }
}

/// Bagian Testing //

const world = new World(50);
world.add_city('Jakarta');
console.log('Random city name:', world.random_city().name);
console.log(
  'Age of first citizen in another random city:',
  world.random_city().citizens
);
console.log('# of Cities:', world.total_cities());

/// Bagian Testing //
